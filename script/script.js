/*ТЕОРЕТИЧНІ ПИТАННЯ

1. Як можна сторити функцію та як ми можемо її викликати?
Функцію можна створити двома способами:
-Function declaration (function nameFunction(parameters){});
-Function expression (const variable = function(){})
Викликати функцію можна вказавши її ім'я і в круглих дужках вказавши відповідні аргументи або просто дужки:
nameFunction();

2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
Це оператор повернення(return) .Він використовується для завершення виконання функції та вказує значення, яке повертається викликачеві.

3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
Параметри - це місцедержателі, вказані в оголошенні функції. Вони представляють значення, які функція очікує отримати під час виклику.
Аргументи - це фактичні значення, що подаються до функції при її виклику. Вони замінюють параметри всередині функції.
Параметри можуть слугувати дефолтними значеннями для функції, якщо не будуть передані аргументи, а аргументи використовуються для отримання від різних умов задач потрібні значення.

4. Як передати функцію аргументом в іншу функцію?
Callback functions (Функції зворотнього виклику в JavaScript - це функції, які передаються як аргументи іншим функціям для виконання в майбутньому. 
Їх часто використовують у сценаріях, де операція є асинхронною або потребує чекати на виникнення події.
*/

/*ПРАКТИЧНІ ПИТАННЯ

1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.*/

'use strict'

let numFirst = prompt('Введіть, будь-ласка, будь-яке число');
while (numFirst != Number(numFirst)) {
    numFirst = prompt('Ви ввели не число! Введіть, будь-ласка, будь-яке число');
}

let numSecond = prompt('Введіть, будь-ласка, ще одне число');
while (numSecond != Number(numSecond)) {
    numSecond = prompt('Ви ввели не число! Введіть, будь-ласка, ще одне число');
}

function divide (numOne, numTwo) {
    return numOne / numTwo;
}
console.log(divide(numFirst, numSecond));


/*2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.*/


let firstNumber = prompt('Введіть, будь-ласка, будь-яке число');
while (firstNumber != Number(firstNumber)) {
    firstNumber = prompt('Ви ввели не число! Введіть, будь-ласка, будь-яке число');
}

let secondNumber = prompt('Введіть, будь-ласка, ще одне число');
while (secondNumber != Number(secondNumber)) {
    secondNumber = prompt('Ви ввели не число! Введіть, будь-ласка, ще одне число');
}

let operation = prompt('Введіть, будь-ласка, одну з цих операцій: +, -, *, /');
let result;

switch (operation) {
    case '+':
        result = Number(firstNumber) + Number(secondNumber);
        break;

    case '-':
        result = firstNumber - secondNumber;
        break;

    case '*':
        result = firstNumber * secondNumber;
        break;

    case '/':
        result = firstNumber / secondNumber;
        break;

    default:
        alert('Такої операції не існує');
    }
        
function calculator (firstNumber, secondNumber, operation) {  
    return result;
}

console.log(`Результат операції - ${result}`)


/*3. Опціонально. Завдання: Реалізувати функцію підрахунку факторіалу числа.*/

let userNumber = prompt('Будь ласка, введіть число для обчислення факторіалу!');
while (userNumber != Number(userNumber)) {
    userNumber = prompt('Ви ввели не число! Введіть, будь-ласка, будь-яке число!');
}
const factorial = (number) => {
    if (number === 0) {
        return 1;
    } else {
        return number * factorial(number - 1);
    }
}

let factorialResult = factorial(userNumber);
console.log(`Факторіал числа ${userNumber} - ${factorialResult}`);